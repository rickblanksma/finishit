import {Howl} from 'howler'

export default {
  install (Vue, options) {
    Vue.prototype.$audio = new Howl(require('@/../static/sounds.json'))
  }
}
