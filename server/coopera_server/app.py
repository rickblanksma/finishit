import os

from flask import Flask
from flask_socketio import SocketIO


def create_app(settings_folder='.'):
    # App
    app = Flask(__name__)
    app.config.from_pyfile(
        os.path.join('..', 'settings', 'common.py'), silent=True)
    app.config.from_pyfile(
        os.path.join(settings_folder, 'local_settings.py'), silent=True)
    socketio = SocketIO(app)
    return app, socketio


app, socketio = create_app('../settings')
