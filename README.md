# Coopera

![Coopera](https://gitlab.com/andresmrm/coopera/raw/master/client/screen.png) 

A cooperative multiplayer game where all players command the same avatar!

Play: https://coopera.ikotema.digital

## Technical

Made with [VueJS](https://vuejs.org) and [Python Flask](http://flask.pocoo.org). Check the README.md in the server and client folders for more information.

Audio project made with [LMMS](https://lmms.io).

The fabfile in this folder may help installing and deploying the server.

## Inspiration

After starting the game I realize that I was probably unconsciously influenced by:

- These [games about social behavior with simple graphics](http://ncase.me)
- These web multiplayer games with simple graphics: [Agar.io](http://agar.io) and [Diep.io](http://diep.io)
- This [color pallete](http://ethanschoonover.com/solarized) (vaguely)

## Docker Hosting

The current docker-compose file is configured to be used in conjunction with [nginx-proxy](https://github.com/jwilder/nginx-proxy) and [docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) using a network named `nginx-proxy`.
Their installation and configuration aren't covered here.

To host Coopera using docker-compose:

1. Copy the `docker-compose.yml` file.
2. Copy the `.env.example` file, rename it to `.env` and change all variables inside accordingly.
3. Create folders `client` and `server` to avoid docker-compose [build validation errors](https://github.com/docker/compose/issues/3391).
4. Run `docker-compose.yml up --no-build`.

If you want to reuse a previous sqlite database, create a `database` folder and place the sqlite database file inside.

## Credits

Made by [Andrés M. R. Martano](https://ikotema.digital/andres).

Counseling and betatesting by Asano (淺野) and Larissa Yuri Oyadomari (親泊).

Audios from [LMMS](https://lmms.io) samples.

[NowRegular](https://fontlibrary.org/en/font/now) font by Alfredo Marco Pradil. 
